# bash_bool_cmp.sh

## General

Bash offers several methods for evaluating boolean conditions. _bash_bool_cmp.sh_ measures the performance.

## Usage

    $ ./bash_bool_cmp.sh -h

    usage: bash_bool_cmp.sh [-t <num>] [-h] [-d]
           -t: perform each test for <num> seconds (default 1)
           -d: switch on debug output
           -h: show this help

## Example

    $ ./bash_bool_cmp.sh
    running each test for 1 second(s).
    testing: if [ "$p" == "true" ]      17311 loops/s
    testing: if [[ "$p" == "true" ]]    19599 loops/s
    testing: if [[ "$p" =~ ^true$ ]]    13192 loops/s
    testing: case "$p" in               20030 loops/s
    testing: if $p                      20227 loops/s
    testing: if (( p ))                 23179 loops/s

    Best performing algo is 'if (( p ))'
