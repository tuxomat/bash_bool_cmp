#!/bin/bash
#
# test the performance of different ways to evaluate
# boolean expressions within bash scripts
#
# author:  automat at posteo.de
# licence: public domain
#
# 2019-05-04:	initial version
# 2019-05-07:	fix typo, rework init of meas.
#

DEFDURATION=1
duration=$DEFDURATION
nrfalses=0
nrtrues=0
debug=0
cnt=0
bestresult=0
bestalgo=''
prog=${0##*/}

function is_valid_num() {
	[[ "$1" -eq 0 ]] && return 1
	[[ "$1" =~ ^[[:digit:]]+$ ]] && return 0
	return 1
}

function die () {
	echo -e "\\n$1\\n" >&2
	exit 1
}

function usage () {
	msg="usage: $prog [-t <num>] [-h] [-d]\\n"
	msg+="       -t: perform each test for <num> seconds (default $DEFDURATION)\\n"
	msg+="       -d: switch on debug output\\n"
	msg+="       -h: show this help"
	die "$msg"
}


function init_measurement () {
	nrfalses=0
	nrtrues=0
	cnt=0
	# wait for begin of next second
	local start=$SECONDS
	until (( SECONDS - start )) ; do true ; done
	SECONDS=0
}

# measure string comparison with single brackets []
function measure_string_if1 () {
	local p

	init_measurement
	while (( duration - SECONDS )) ; do
		(( RANDOM & 1 )) && p='true' || p='false'
		if [ "$p" == "true" ] ; then
		    (( ++nrtrues ))
		else
		    (( ++nrfalses ))
		fi
		(( ++cnt ))
	done
}

# measure string comparison with double brackets [[]]
function measure_string_if2 () {
	local p

	init_measurement
	while (( duration - SECONDS )) ; do
		(( RANDOM & 1 )) && p='true' || p='false'
		if [[ "$p" == "true" ]] ; then
		    (( ++nrtrues ))
		else
		    (( ++nrfalses ))
		fi
		(( ++cnt ))
	done
}

# measure comparison with case..esac
function measure_case () {
	local p

	init_measurement
	while (( duration - SECONDS )) ; do
		(( RANDOM & 1 )) && p='true' || p='false'
		case "$p" in
		"true") (( ++nrtrues )) ;;
		*)      (( ++nrfalses )) ;;
		esac
		(( ++cnt ))
	done
}

# measure comparison with regular expression =~
function measure_regex () {
	local p

	init_measurement
	while (( duration - SECONDS )) ; do
		(( RANDOM & 1 )) && p='true' || p='false'
		if [[ "$p" =~ ^true$ ]] ; then
		    (( ++nrtrues ))
		else
		    (( ++nrfalses ))
		fi
		(( ++cnt ))
	done
}

# measure comparison with true/false builtins
function measure_truefalse () {
	local p

	init_measurement
	while (( duration - SECONDS )) ; do
		(( RANDOM & 1 )) && p='true' || p='false'
		if $p ; then
		    (( ++nrtrues ))
		else
		    (( ++nrfalses ))
		fi
		(( ++cnt ))
	done
}

# measure comparison with numerical evaluation
function measure_numerical () {
	local p

	init_measurement
	while (( duration - SECONDS )) ; do
		(( RANDOM & 1 )) && p=1 || p=0
		if (( p )) ; then
		    (( ++nrtrues ))
		else
		    (( ++nrfalses ))
		fi
		(( ++cnt ))
	done
}

# top level function - calls a single measuring function
function measure () {
	printf "testing: %-25s" "$1"
	$2
	if (( debug )) ; then
	    printf "%7d loops/s  [%d/%d]\n" $(( cnt / duration )) $nrtrues $nrfalses
	else
	    printf "%7d loops/s\n" $(( cnt / duration ))
	fi

	if [[ $bestresult -lt $cnt ]] ; then
		bestresult=$cnt
		bestalgo="$1"
	fi
}


# main

trap 'echo -e "\\ncaught signal, exiting..."; trap - INT TERM QUIT; exit 2'  INT TERM QUIT
stime=$EPOCHSECONDS
[[ -z $stime ]] && stime=$SECONDS

while getopts ':hdt:' param ; do
	case "$param" in
	d)	debug=1 ;;
	t)	duration=$OPTARG ;;
	h)	usage ;;
	:)	die "missing argument to '$OPTARG'. See $prog -h for help" ;;
	?)	die "unknown option '$OPTARG'. See $prog -h for help" ;;
	*)	usage ;;
	esac
done

is_valid_num "$duration" || die "bad number $duration - please give an integer greater then zero"
dur=$(( duration )) || die "bad number"
[[ -z $dur ]] && die "bad number"
duration=$dur
echo "running each test for $duration second(s)."

#      | text to show            | function to call
#------+-------------------------+---------------------
# shellcheck disable=SC2016
measure 'if [ "$p" == "true" ]'   'measure_string_if1'
# shellcheck disable=SC2016
measure 'if [[ "$p" == "true" ]]' 'measure_string_if2'
# shellcheck disable=SC2016
measure 'if [[ "$p" =~ ^true$ ]]' 'measure_regex'
# shellcheck disable=SC2016
measure 'case "$p" in'            'measure_case'
# shellcheck disable=SC2016
measure 'if $p'                   'measure_truefalse'
measure 'if (( p ))'              'measure_numerical'

echo -e "\\nBest performing algo is '$bestalgo'\\n"

(( debug )) && {
	etime=$EPOCHSECONDS
	[[ -z $etime ]] && etime=$SECONDS
	rtime=$(( etime - stime ))
	printf "\ntotal runtime:  %25d s\n\n" $rtime
}

exit 0

